<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@index')->name('home'); // Главная
Route::get('news', 'NewsController@index')->name('news.index'); // Все новости
Route::get('news/{news}', 'NewsController@show')->name('news.show'); // Просмотр новости
Route::get('gallery', 'GalleryController@index')->name('gallery'); // Галерея

Route::resources([
    'admin-news' => 'Admin\NewsController', // Редактирование новостей
    'admin-gallery' => 'Admin\GalleryController', // Редактирование галереи
]);
Route::get('admin-options', 'Admin\OptionsController@edit')->name('admin-options'); // Настройка главной станицы
Route::post('admin-options', 'Admin\OptionsController@update');

