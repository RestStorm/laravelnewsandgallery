@extends('layouts.admin')

@section('title', 'Настройки')

@section('content')
    <div class="row">
        <fieldset class="m-2">
            {{ Form::open(['route' => ['admin-options'], 'method' => 'POST']) }}
            <div class="form-group">
                {{ Form::label('title', 'Текст главной страницы') }}
                {{ Form::textarea('homepageContent', $homepageContent->value, ['class' => 'form-control']) }}
            </div>
            <div class="float-left">
                {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </fieldset>
    </div>
@endsection
