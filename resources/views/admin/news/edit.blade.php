@extends('layouts.admin')

@section('title', 'Новости')

@section('content')
    <fieldset class="m-2">
        <a href="{{route('admin-news.index')}}">Назад</a>
        {{ Form::model($post, ['route' => ['admin-news.update', $post->id], 'method' => 'PUT']) }}
        <div class="form-group">
            {{ Form::label('title', 'Заголовок') }}
            {{ Form::text('title', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('description', 'Описание') }}
            {{ Form::text('description', null, ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('content', 'Контент') }}
            {{ Form::textArea('content', null, ['class' => 'form-control']) }}
        </div>
        <div class="float-left">
            {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
        </div>
        {{ Form::close() }}

        {{ Form::open(['route' => ['admin-news.destroy', $post->id], 'method' => 'DELETE']) }}
        <div class="float-right">
            {{ Form::submit('Удалить', ['class' => 'btn btn-danger']) }}
        </div>
        {{ Form::close() }}
    </fieldset>
@endsection
