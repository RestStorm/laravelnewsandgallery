@extends('layouts.admin')

@section('title', 'Новости')

@section('content')
    <div class="m-2">
        <a href="{{ route('admin-news.create') }}" class="btn btn-primary">Создать новость</a>
    </div>
    <table class="table news-table">
        <thead>
        <tr>
            <td class="ids">#</td>
            <td>Заголовок</td>
            <td class="date">Дата</td>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->id }}</td>
                <td><a href="{{ route('admin-news.edit', $post->id) }}">{{ $post->title }}</a></td>
                <td>{{ $post->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
