@extends('layouts.admin')

@section('title', 'Новости')

@section('content')
    <fieldset class="m-2">
        {!! Form::open(['route' => 'admin-news.store'])!!}
            <a href="{{route('admin-news.index')}}">Назад</a>
            <div class="form-group">
                {{ Form::label('title', 'Заголовок') }}
                {{ Form::text('title', null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('description', 'Описание') }}
                {{ Form::text('description', null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('content', 'Контент') }}
                {{ Form::textArea('content', null, ['class' => 'form-control']) }}
            </div>
            {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
        {!! Form::close() !!}
    </fieldset>
@endsection
