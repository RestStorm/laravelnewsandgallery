@extends('layouts.admin')

@section('title', 'Настройки')

@section('content')
    <section>
        <fieldset class="m-2">
            {{ Form::open(['route' => ['admin-gallery.store'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) }}
            <div class="form-group">
                {{ Form::label('title', 'Дбавить изабражение') }}
                {{ Form::file('image', ['class' => 'form-control']) }}
            </div>
            <div class="float-left">
                {{ Form::submit('Загрузить', ['class' => 'btn btn-primary']) }}
            </div>
            {{ Form::close() }}
        </fieldset>
    </section>
    <section id="gallery">
        <div class="row">
            @foreach($images as $image)
                <div class="col-md-3">
                    <div class="card">

                        <img src="{{config('images.directory') . 'resize/' . $image->path}}"
                             class="card-img-top">
                        <div class="card-body">

                            {{ Form::open(['route' => ['admin-gallery.destroy', $image->id], 'method' => 'DELETE']) }}
                            {{ Form::submit('Удалить', ['class' => 'btn btn-primary']) }}
                            {{ Form::close() }}
                        </div>
                    </div>

                </div>
            @endforeach
        </div>
    </section>
@endsection
