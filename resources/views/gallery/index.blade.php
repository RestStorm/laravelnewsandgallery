@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="gallery">
            <div class="row">
                @foreach($images as $image)
                    <div class="col-md-3">
                        <img src="{{config('images.directory') . 'resize/' . $image->path}}"
                             class="card-img-top modalImg img-thumbnail"
                             data-original="{{config('images.directory')  . $image->path }}">
                    </div>
                @endforeach
            </div>
        </section>
    </div>

    <div id="modal" class="modal">
        <img class="modal-content" id="modalImg">
    </div>
@endsection

@section('js')
    <script>
        $('body').on('click', '.modalImg', function () {
            let modal = $('#modal');
            let modalImg = $('#modalImg');
            modal.css({'display': 'block'});
            modalImg.attr('src', $(this).data('original'));
        });

        $('.modal').click(function () {
            $('#modal').css({'display': 'none'});

        });
    </script>
@endsection