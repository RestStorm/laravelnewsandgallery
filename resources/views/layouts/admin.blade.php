<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <title>@yield('title')</title>
</head>
<body>

<header class="navbar navbar-dark bg-dark navbar-expand flex-column flex-md-row bd-navbar">
    <div class="navbar-nav-scroll">
        <ul class="navbar-nav bd-navbar-nav flex-row">
            <li class="nav-item">
                <a class="nav-link " href="/">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin-options') }}">Настройки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{ route('admin-news.index') }}">Новости</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " href="{{ route('admin-gallery.index') }}">Галерея</a>
            </li>
        </ul>
    </div>

    <ul class="navbar-nav flex-row ml-md-auto d-md-flex">
        <a class="btn d-lg-inline-block mb-3 mb-md-0 ml-md-3" href="{{ route('logout') }}}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Выйти
        </a>
        <form id="logout-form" action="http://127.0.0.1:8000/logout" method="POST" style="display: none;">
            @csrf
        </form>
        </div>
    </ul>

</header>
<div class="container">
    @if ($errors->hasBag())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
            @endforeach
        </div>
    @endif
        @yield('content')
</div>
<script src="{{ asset('js/jquery-3.2.1.slim.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
@yield('js')
</body>
</html>