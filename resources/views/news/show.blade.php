@extends('layouts.app')

@section('content')
    <div class="container news">
        <div class="m-2">
            <a href="{{ route('news.index') }}">Назад</a>
            <h2 class="text-center m-2">{{ $post->title }}</h2>
            <div class="content">{!! $post->content !!}</div></p>
        </div>
    </div>
@endsection
