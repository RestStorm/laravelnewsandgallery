@extends('layouts.app')

@section('content')
    <div class="container news">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-lg-4 p-5 text-center">
                    <h2 >{{ $post->title }}</h2>
                    <p class="text-left">{{ $post->description }}</p>
                    <a href="{{ route('news.show', $post->id) }}">Подребнее</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection
