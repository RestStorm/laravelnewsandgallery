<?php

namespace App\Http\Controllers\Admin;

use App\Models\Images;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class GalleryController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'images' => Images::all(),
        ];
        return view('admin.gallery.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $path = public_path(config('images.directory'));
        File::makeDirectory($path . '/resize', $mode = 0777, true, true);

        $this->validate($request, ['image' => 'required|image']);

        $file = $request->file('image');
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $file->move($path, $filename);

        $file = Image::make($path . '/' . $filename);
        $file->resize(320, 240)->save($path . 'resize/' . $filename);

        $image = New Images();
        $image->path = $filename;
        $image->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = Images::find($id);
        $path = public_path(config('images.directory'));

        File::delete([$path . $image->path, $path . 'resize/' . $image->path]);
        Images::destroy($id);

        return redirect()->route('admin-gallery.index');
    }
}
