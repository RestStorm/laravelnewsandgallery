<?php

namespace App\Http\Controllers\Admin;

use App\Models\Options;
use Illuminate\Http\Request;

class OptionsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data = [
            'homepageContent' => Options::getHomepageContent(),
        ];

        return view('admin.options.index', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, Options::$rules);

        $post = Options::getHomepageContent();
        $post->value = $request->input('homepageContent');
        $post->save();
        return redirect()->back();
    }
}
