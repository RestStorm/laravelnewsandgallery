<?php

namespace App\Http\Controllers;

use App\Models\Images;

class GalleryController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'images' => Images::all(),
        ];
        return view('gallery.index', $data);
    }
}
