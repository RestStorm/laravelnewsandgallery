<?php

namespace App\Http\Controllers;


use App\Models\Options;

class IndexController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'homepageContent' => Options::getHomepageContent(),
        ];

        return view('index.index', $data);
    }
}
