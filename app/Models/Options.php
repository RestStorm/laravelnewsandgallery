<?php
/**
 * Created by PhpStorm.
 * User: deida
 * Date: 27.08.2018
 * Time: 7:19
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Options
 * @property int id
 * @property string alias
 * @property string value
 * @package App\Models
 */
class Options extends Model
{
    protected $table = 'options';
    static $rules = ['homepageContent' => 'required'];

    /**
     *  Вернет контент главной страницы
     *
     * @return mixed
     */
    static function getHomepageContent(){
        return self::where('alias', 'homepageContent')->first();
    }
}