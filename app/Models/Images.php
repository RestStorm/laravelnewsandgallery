<?php
/**
 * Created by PhpStorm.
 * User: deida
 * Date: 27.08.2018
 * Time: 7:19
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Images
 * @property int id
 * @property string path
 * @package App\Models
 */
class Images extends Model
{
    protected $table = 'images';
    static $rules = [];
}