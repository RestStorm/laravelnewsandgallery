<?php
/**
 * Created by PhpStorm.
 * User: deida
 * Date: 27.08.2018
 * Time: 3:42
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class News
 * @property int $id
 * @property string $title
 * @property string description
 * @property string content
 * @package App\Models
 */
class News extends Model
{
    protected $table = 'news';

    static $rules = [
        'title' => 'required|max:255',
        'description' => 'required|max:255',
        'content' => 'required',
        ];
}
